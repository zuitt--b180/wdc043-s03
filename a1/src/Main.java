import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        HashMap<String, Integer> games = new HashMap<>();
        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);
//        System.out.println(games);

        games.forEach ((key, value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();
        games.forEach ((key, value) -> {

            if (value <= 30){
                System.out.println(key + " has been added to the top games list!");
                topGames.add(key);
            }
        });
        System.out.println("Our Shop's top games:");
        System.out.println(topGames);



    }
}